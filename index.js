/**
 * Created by user on 1/18/2017.
 */

let express = require('express');
let path = require('path');
let app = express();

// Define the port to run on
app.set('port', 80);

app.use(express.static(path.join(__dirname, '/')));

// Listen for requests
let server = app.listen(app.get('port'), function() {
    let port = server.address().port;
    console.log(`Directory used: ${__dirname}`);
    console.log('Magic happens on port ' + port);
});


